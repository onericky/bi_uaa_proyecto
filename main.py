from json import dumps
from bokeh.io import output_file, show
from bokeh.models import ColumnDataSource, GMapOptions
from bokeh.plotting import gmap, figure, output_file, show
import pymongo
import xlwt
import csv
import dns
import numpy as np
import scipy.stats
from sklearn.linear_model import LinearRegression

def data():
    client = pymongo.MongoClient("mongodb+srv://sigmotoa:clavemongoBI@cluster0.yhlxy.mongodb.net/BI_Project?retryWrites=true&w=majority")
    db = client.BI_Project

    user = db.user
    review = db.review
    business = db.business
    checkin = db.checkin

    def bestWorstBusinessCategories():
        result = business.aggregate([
            {
                "$group": {
                    "_id": "$categories",
                    "count": { "$sum": 1},
                    "sum_stars": {"$sum": "$stars"},
                    "avg_stars": {"$avg": "$stars"}
                }
            },
            {
                "$sort": {
                    "count": -1
                }
            }
        ])

        data = {}
        for x in result:
            categories = ' | '
            categories = categories.join(x['_id'])
            data[categories] = {'count': x['count'],
                                 'sum_stars': x['sum_stars'],
                                 'avg_stars': x['avg_stars']
                                }

        def saveBestCategories(data):
            data2 = sorted(data.items(), key=lambda x: (x[1]['avg_stars'], x[1]['count']), reverse=True)
            book = xlwt.Workbook(encoding="utf-8")
            sheet1 = book.add_sheet("Sheet 1")
            sheet1.write(0, 0, "Categoria")
            sheet1.write(0, 1, "Cantidad de negocios")
            sheet1.write(0, 2, "Promedio de evaluacion")
            sheet1.write(0, 3, "Suma de evaluaciones")
            c = 0
            for i in range(len(data2)):
                if data2[i][1]['count'] <= 5:
                    continue
                c += 1
                sheet1.write(c, 0, data2[i][0])
                sheet1.write(c, 1, data2[i][1]['count'])
                sheet1.write(c, 2, data2[i][1]['avg_stars'])
                sheet1.write(c, 3, data2[i][1]['sum_stars'])

                if c >= 10:
                    break
            book.save("top10-bestCategories.xls")

        def saveWorstCategories(data):
            data2 = sorted(data.items(), key=lambda x: (x[1]['avg_stars'], x[1]['count']), reverse=False)
            book = xlwt.Workbook(encoding="utf-8")
            sheet1 = book.add_sheet("Sheet 1")
            sheet1.write(0, 0, "Categoria")
            sheet1.write(0, 1, "Cantidad de negocios")
            sheet1.write(0, 2, "Promedio de evaluacion")
            sheet1.write(0, 3, "Suma de evaluaciones")
            c = 0
            for i in range(len(data2)):
                if data2[i][1]['count'] <= 5:
                    continue
                c += 1
                sheet1.write(c, 0, data2[i][0])
                sheet1.write(c, 1, data2[i][1]['count'])
                sheet1.write(c, 2, data2[i][1]['avg_stars'])
                sheet1.write(c, 3, data2[i][1]['sum_stars'])

                if c >= 10:
                    break
            book.save("top10-worstCategories.xls")

        saveBestCategories(data)
        saveWorstCategories(data)

    def getBusinessDataByStars(stars = 5, dataResult = []):
        gte = 4.5
        lt = 5.1
        if stars == 4:
            gte = 3.5
            lt = 4.5
        elif stars == 3:
            gte = 2.5
            lt = 3.5
        elif stars == 2:
            gte = 1.5
            lt = 2.5
        elif stars == 1:
            gte = 0
            lt = 1.5
        else:
            stars = 5

        result = business.aggregate([
            {
                "$match": {
                    "stars": {"$gte": gte, "$lt": lt}
                }
            },
            {
                "$lookup":
                {
                    "from": "checkin",
                    "localField": "business_id",
                    "foreignField": "business_id",
                    "as": "checkin_data"
                }
            },
            {
                "$lookup":
                {
                    "from": "review",
                    "localField": "business_id",
                    "foreignField": "business_id",
                    "as": "review_data"
                }
            }
        ])

        data = {}
        for x in result:
            sum = 0
            if len(x['checkin_data']) > 0:
                for kci, vci in x['checkin_data'][0]['checkin_info'].items():
                    sum += vci

            try:
                data[stars]['total_review'] += len(x['review_data'])
                data[stars]['total_checkin'] += sum
            except KeyError:
                data[stars] = {'total_review': len(x['review_data']),
                               'total_checkin': sum
                              }

        def saveBusinessDataByStars(data):
            book = xlwt.Workbook(encoding="utf-8")
            sheet1 = book.add_sheet("Sheet 1")
            sheet1.write(0, 0, "Estrellas")
            sheet1.write(0, 1, "Total de reviews")
            sheet1.write(0, 2, "Total de checkin")
            for p in range(len(data)):
                for k, v in data[p].items():
                    sheet1.write(p + 1, 0, k)
                    sheet1.write(p + 1, 1, v['total_review'])
                    sheet1.write(p + 1, 2, v['total_checkin'])

            book.save("businessCheckinReviewDataByStars.xls")

        dataResult.append(data)
        # if stars > 1 and stars <= 5:
        if 1 < stars <= 5:
            getBusinessDataByStars(stars - 1, dataResult)
        if stars == 1:
            saveBusinessDataByStars(dataResult)

    def showInMap():
        '''Show in google maps the location of the places with the inserted number of starts '''
        lati = []
        long = []

        colors = {0: "green", 1: "blue", 2: "red", 3: "pink", 4: "orange", 5: "grey", 6: "white"}

        def lookSites(i):
            lati.clear()
            long.clear()
            for x in (business.find({"stars": {"$eq": i}},
                                    {'name': 1, "_id": 0, 'business_id': 1, 'longitude': 1, 'latitude': 1})):
                lati.append(x['latitude'])
                long.append(x['longitude'])

            lat_r, lon_r = lati, long
            return lat_r, lon_r

        for j in range(0,6):
            output_file("stars_" + str(j) + ".html")
            map_options = GMapOptions(lat=33.5, lng=-112, map_type="roadmap", zoom=9)
            p = gmap('AIzaSyBQL1UkU24maUnOMhBRUi3imiR_9OBz1Mg', map_options, title="Arizona "+str(j)+" stars")

            s1 = lookSites(j)
            source1 = ColumnDataSource(
                data=dict(lat=s1.__getitem__(0),
                          lon=s1.__getitem__(1))
            )
            p.cross(x="lon", y="lat", size=5, fill_color=colors.__getitem__(j), fill_alpha=0.8, source=source1)
            show(p)

    def lookChecks():
        check_data = dict()
        for x in (checkin.find({},{"business_id":1,"checkin_info":1,"_id":0})):
          check_data.__setitem__(str(x['business_id']),x['checkin_info'])

        return check_data

    def countChecks(info):
        counted = dict()
        for element in info.items():
            count = dict(element.__getitem__(1))
            total = 0
            for item in count.values():
                total = total+item
            counted.__setitem__(element.__getitem__(0), total)
        return counted

    def getBusinessData():
        result = business.aggregate([
            {
                "$lookup":
                {
                    "from": "checkin",
                    "localField": "business_id",
                    "foreignField": "business_id",
                    "as": "checkin_data"
                }
            },
            {
                "$lookup":
                {
                    "from": "review",
                    "localField": "business_id",
                    "foreignField": "business_id",
                    "as": "review_data"
                }
            },
            {
                "$group": {
                    "_id": "$business_id",
                    "business_name": {"$first": "$name"},
                    "count": {"$sum": 1},
                    "stars": {"$first": "$stars"},
                    "total_review": {"$first": {"$size": "$review_data"}},
                    "checkin_data": {"$first": "$checkin_data"}
                }
            },
            {
                "$sort": {
                    "count": -1
                }
            }
        ])

        data = {}
        for x in result:
            sum = 0
            if len(x['checkin_data']) > 0:
                for kci, vci in x['checkin_data'][0]['checkin_info'].items():
                    sum += vci

                data[x['_id']] = {
                    'business_name': x['business_name'],
                    'total_checkin': sum,
                    'total_review': x['total_review'],
                    'stars': x['stars']
                }


        def saveBusinessData(data):
            file = open('businessData.csv', 'w', newline='')

            with file:
                # identifying header
                header = ['Business_id', 'Business_name', 'Total_checkin', 'Total_review', 'Stars']
                writer = csv.DictWriter(file, fieldnames=header)
                writer.writeheader()
                for row in data.items():
                    # writing data row-wise into the csv file
                    writer.writerow({'Business_id': row[0],
                                     'Business_name': row[1]["business_name"],
                                     'Total_checkin': row[1]["total_checkin"],
                                     'Total_review': row[1]["total_review"],
                                     'Stars': row[1]["stars"]})

        saveBusinessData(data)
        linearRegression()

    def linearRegression():

        def estimation_b_A():
            csvdata = np.genfromtxt('businessData.csv',names= True, delimiter=',', dtype=(str,int,int,float))

            check = []
            review = []
            stars = []
            for element in range(len(csvdata)):

                check.append(csvdata[element][1])
                review.append(csvdata[element][2])
                stars.append(csvdata[element][3])

            #Looking for averages

            y_stars = np.array(stars)
            x_check = np.array(check)
            x_review = np.array(review)

            m_checkins_x, m_reviews_x = np.mean(x_check), np.mean(x_review),
            m_stars_y = np.mean(y_stars)

            #looking for correlation

            cor_chek_stars= np.corrcoef(x_check,y_stars)
            cor_revi_stars= np.corrcoef(x_review,y_stars)

            #Looking for standard deviation

            std_check = np.std(x_check)
            std_review = np.std(x_review)
            std_stars = np.std(y_stars)

            # total_xy_checkins = np.sum((check-m_checkins_x)*(stars-m_stars_y))
            # total_xy_reviews = np.sum((review-m_reviews_x)*(stars-m_stars_y))
            # total_xx_checkins = np.sum(check*(check-m_checkins_x))
            # total_xx_reviews = np.sum(review*(review-m_reviews_x))
            # A_checkin = total_xy_checkins / total_xx_checkins
            # b_checkin = m_stars_y - A_checkin*m_checkins_x
            # A_reviews = total_xy_reviews / total_xx_reviews
            # b_reviews =m_stars_y - A_reviews*m_reviews_x

            #calculation for chekin
            checkin_values = scipy.stats.linregress(x_check, y_stars)
            A_checkin = checkin_values.slope
            b_checkin = checkin_values.intercept

            #calculation for reviews
            review_values = scipy.stats.linregress(x_review, y_stars)
            A_reviews = review_values.slope
            b_reviews = review_values.intercept

            # b_checkin = cor_chek_stars * (std_stars/std_check)
            # b_reviews = cor_revi_stars * (std_stars/std_review)

            #calculation of a
            # A_checkin = m_stars_y - (b_checkin * m_checkins_x)
            # A_reviews = m_stars_y - (b_reviews * m_reviews_x)

            return (A_checkin, b_checkin, x_check, A_reviews, b_reviews, x_review, y_stars)

        def graphic(b):
            y_pred_checkin = b[0] + b[1]* b[2]
            y_pred_review = b[4] + b[3]*b[5]

            plot_checkin = figure(plot_width=500, plot_height=500)
            plot_checkin.asterisk(x=b[2], y=b[6], size= 10, color='green')
            plot_checkin.line(x=b[2] ,y=y_pred_checkin)

            plot_review = figure(plot_width=500, plot_height=500)
            plot_review.asterisk(x=b[5], y=b[6], size=10, color="red")
            plot_review.line(x=b[5], y=y_pred_review)


            show(plot_checkin)
            show(plot_review)

            regression_lineal = LinearRegression()

            regression_lineal.fit(b[2].reshape(-1,1),b[6])

            print("b: " + str(b[1]) + ", A = " + str(b[0]))
            print("b: "+str(regression_lineal.coef_)+", A = "+str(regression_lineal.intercept_))
        graphic(estimation_b_A())




    getBusinessData()
    # print(countChecks(lookChecks()))
    # showInMap()
    # bestWorstBusinessCategories()
    # getBusinessDataByStars()

if __name__ == '__main__':
    data()
